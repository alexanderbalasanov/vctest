package com.vc.vctest.utils;

import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;

import androidx.annotation.NonNull;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

public class VCQueue {

    private HandlerThread operationThread ;
    private Handler operationHandler ;
    private static Handler mainThread = new Handler(Looper.getMainLooper());

    public VCQueue(String name){
        final CountDownLatch latch = new CountDownLatch(1);
        operationThread = new HandlerThread(name) {
            @Override
            protected void onLooperPrepared() {
                super.onLooperPrepared();
                operationHandler = new Handler(getLooper()){
                    @Override
                    public void handleMessage(@NonNull Message msg) {
                        if(msg.what == 1){
                            ((Runnable)msg.obj).run();
                        }
                    }
                };
                latch.countDown();
            }
        };
        operationThread.start();
        try {
            latch.await(100, TimeUnit.MILLISECONDS);
        } catch (Exception err) {
            operationHandler = null;
            err.printStackTrace();
        }
    }

    public void post(final Runnable completion) {
        Message message = Message.obtain() ;
        message.obj = completion ;
        message.what = 1 ;
        operationHandler.post(completion) ;
    }

    public void postMain(final Runnable completion) {
        mainThread.post(completion);
    }

}
