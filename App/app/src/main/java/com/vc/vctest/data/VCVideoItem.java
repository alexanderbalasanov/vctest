package com.vc.vctest.data;

import java.io.Serializable;

public class VCVideoItem implements Serializable {

    private String title ;
    private String thumbUrl ;
    private String id ;

    /**
     * Single item include information about video
     * @param title
     * @param thumbUrl
     * @param id
     */
    public VCVideoItem(String title, String thumbUrl, String id) {
        this.title = title ;
        this.thumbUrl = thumbUrl ;
        this.id = id ;
    }

    /***
     * Get video title string
     * @return
     */
    public String getTitle(){
        return title ;
    }

    /**
     * Get thumbnail url
     * @return
     */
    public String getThumbUrl(){
        return thumbUrl ;
    }

    /***
     * Get video ID
     * @return
     */
    public String getVideoId(){
        return id ;
    }
}
