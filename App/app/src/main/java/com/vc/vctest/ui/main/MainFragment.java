package com.vc.vctest.ui.main;

import androidx.databinding.Observable;
import androidx.lifecycle.ViewModelProvider;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.google.android.youtube.player.YouTubeStandalonePlayer;
import com.vc.vctest.R;
import com.vc.vctest.data.VCVideoItem;

import java.lang.ref.WeakReference;

public class MainFragment extends Fragment implements MainViewModel.SelectedItemListener{
    public static final String DEVELOPER_KEY = "AIzaSyBbcj895SelbMsjMBWdLaum2k2V6ER32rQ";
    private MainViewModel mViewModel;
    private RecyclerView itemsList ;
    private ProgressBar loader ;
    public static MainFragment newInstance() {
        return new MainFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,@Nullable Bundle savedInstanceState) {

        View view =  inflater.inflate(R.layout.main_fragment, container, false);
        itemsList = view.findViewById(R.id.list) ;
        loader = view.findViewById(R.id.loader) ;
        itemsList.setLayoutManager(new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL, false));
        itemsList.setNestedScrollingEnabled(false);

        return view ;
    }


    @Override
    public void onResume() {
        super.onResume();
        if(mViewModel == null) {
            WeakReference<MainFragment> THIS = new WeakReference<>(MainFragment.this) ;
            mViewModel = new ViewModelProvider(this).get(MainViewModel.class);
            mViewModel.setContext(getContext());
            mViewModel.start(new Observable.OnPropertyChangedCallback() {
                @Override
                public void onPropertyChanged(Observable sender, int propertyId) {
                    MainFragment self = THIS.get() ;
                    if(self != null){
                        self.loader.setVisibility(View.GONE);
                        if(!self.mViewModel.getItems().isEmpty()) {
                            self.itemsList.setAdapter(new VCItemAdapter(mViewModel, self));
                        }
                    }
                }
            }) ;
        }
    }

    /***
     * Receiving when user click on an item in the list
     * @param item
     */
    @Override
    public void onSelected(VCVideoItem item) {
        try {
            Intent intent = YouTubeStandalonePlayer.createVideoIntent(getActivity(),DEVELOPER_KEY,item.getVideoId()) ;
            startActivity(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}