package com.vc.vctest.ui.main;

import android.graphics.drawable.BitmapDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.databinding.Observable;
import androidx.recyclerview.widget.RecyclerView;

import com.vc.vctest.R;
import com.vc.vctest.data.VCVideoItem;

import org.jetbrains.annotations.NotNull;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

public class VCItemAdapter extends RecyclerView.Adapter{
    private ArrayList<VCVideoItem> items  ;
    private MainViewModel model ;

    public VCItemAdapter(@NotNull @NonNull MainViewModel model,MainViewModel.SelectedItemListener listener) {
        this.model = model ;
        this.items = model.getItems() ;
        WeakReference<VCItemAdapter> THIS = new WeakReference<>(VCItemAdapter.this) ;

        model.addContentChangeObserver(()->{
            VCItemAdapter adapter = THIS.get();
            if(adapter != null){
                adapter.notifyDataSetChanged();
            }
        });

        model.addSelectionListener(listener);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
       View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_view_item, parent, false);
        final VCItemAdapter.VCItemHolder holder = new VCItemAdapter.VCItemHolder(view);
        WeakReference<VCItemAdapter> THIS = new WeakReference<>(VCItemAdapter.this) ;
        view.setOnClickListener(view1 -> {
            VCItemAdapter adapter = THIS.get();
            if(adapter != null){
                adapter.model.setSelected(holder.vcVideoItem);
            }
        });
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        VCItemHolder h = (VCItemHolder)holder ;
        h.bind(items.get(position));
    }

    @Override
    public int getItemCount() {
        return items.size();
    }


    class VCItemHolder extends RecyclerView.ViewHolder {

        private ImageView thubmnail ;
        private TextView description ;
        private ProgressBar loader ;
        private VCVideoItem vcVideoItem ;
        public VCItemHolder(@NonNull View itemView) {
            super(itemView);
            thubmnail = itemView.findViewById(R.id.thumbnail);
            description = itemView.findViewById(R.id.description);
            loader = itemView.findViewById(R.id.loader);

        }

        public void bind(VCVideoItem vcVideoItem) {
            this.vcVideoItem = vcVideoItem ;
            BitmapDrawable bitmap = model.getImage(vcVideoItem.getThumbUrl()) ;
            if(bitmap == null){
                model.loadImage(vcVideoItem.getThumbUrl()) ;
            }
            if(bitmap != null){
                thubmnail.setBackground(bitmap);
                loader.setVisibility(View.GONE);
            }

            description.setText(vcVideoItem.getTitle());
        }
    }
}
