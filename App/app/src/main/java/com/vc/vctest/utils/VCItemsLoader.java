package com.vc.vctest.utils;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;

public class VCItemsLoader {
    private static Exception lastException = null ;
    /***
     * Load jsonobject
     * Note: This is sync method and must be not call on main thread.
     * @param path
     * @return
     * @throws IOException
     */
    public static JSONObject load(String path) {

        BufferedReader bufferedReader = null;
        HttpURLConnection urlConnection = null ;
        try {


            SSLContext sslContext = SSLContext.getInstance("TLS");
            sslContext.init(null, null, null);
            URL url = new URL(path);
            urlConnection = (HttpURLConnection) url.openConnection();

            if (urlConnection instanceof HttpsURLConnection) {
                ((HttpsURLConnection) urlConnection).setSSLSocketFactory(sslContext.getSocketFactory());
            }

            urlConnection.setUseCaches(false);
            urlConnection.setConnectTimeout(90000);
            urlConnection.setReadTimeout(90000);
            urlConnection.setRequestMethod("GET");
            urlConnection.setDefaultUseCaches(false);
            urlConnection.setDoInput(true);

            int responseCode = urlConnection.getResponseCode();

            StringBuilder sb = new StringBuilder();

            if (HttpURLConnection.HTTP_OK <= responseCode && responseCode < HttpURLConnection.HTTP_MULT_CHOICE) {
                InputStream is = urlConnection.getInputStream();
                if (is != null) {
                    InputStreamReader isr = new InputStreamReader(is);
                    bufferedReader = new BufferedReader(isr);
                }
            } else {
                InputStream is = urlConnection.getErrorStream();
                if (is != null) {
                    InputStreamReader isr = new InputStreamReader(is);
                    bufferedReader = new BufferedReader(isr);
                }
            }
            String line;
            if (bufferedReader != null) {
                while ((line = bufferedReader.readLine()) != null) {
                    sb.append(line);
                }
            }
            return new JSONObject(sb.toString());
        } catch (Exception err) {
            err.printStackTrace();
            lastException = err ;
        }
        finally {
            if (bufferedReader != null) {
                try {
                    bufferedReader.close();
                } catch (IOException e) {/* do nothing */}
            }
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
        }
        return null ;
    }

    /**
     * Get last know exeption
     * @return
     */
    public static Exception getLastException(){
        Exception last = lastException ;
        lastException = null ;
        return last ;
    }
}
