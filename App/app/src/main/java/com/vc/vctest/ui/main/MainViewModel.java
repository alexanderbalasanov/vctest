package com.vc.vctest.ui.main;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.webkit.URLUtil;

import androidx.annotation.NonNull;
import androidx.databinding.Observable;
import androidx.databinding.ObservableField;
import androidx.lifecycle.ViewModel;


import com.vc.vctest.data.VCVideoItem;
import com.vc.vctest.utils.VCItemsLoader;
import com.vc.vctest.utils.VCQueue;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Hashtable;

public class MainViewModel extends ViewModel {

    public interface SelectedItemListener {
        void onSelected(VCVideoItem item);
    }

    private static final String SOURCE = "https://landing.cal-online.co.il/youtube/playlists.json" ;
    private static final String TAG = "MainViewModel" ;
    private VCQueue networkQueue = new VCQueue(TAG);
    private ObservableField<ArrayList<VCVideoItem>> itemsData = new  ObservableField<>(new ArrayList<>());
    private ObservableField<Hashtable<String,BitmapDrawable>> images = new  ObservableField<>(new Hashtable<>());
    private ObservableField<VCVideoItem> selectedItemInList = new ObservableField(null);
    private Runnable contentChangeListener ;

    private Context context ;
    /***
     * Init and start items loading on separate thread
     */
    public void start(Observable.OnPropertyChangedCallback listener) {
        itemsData.addOnPropertyChangedCallback(listener);
        WeakReference<MainViewModel> THIS = new WeakReference<>(MainViewModel.this);
        networkQueue.post(()->{
            MainViewModel model = THIS.get();
            if(model !=null){
                model.onStart(THIS);
            }
        });
    }

    public ArrayList<VCVideoItem> getItems(){
        return itemsData.get() ;
    }

    /****
     * Loading and update UI about loaded items
     */
    private void onStart(WeakReference<MainViewModel> THIS){
        try {

            JSONObject json = VCItemsLoader.load(SOURCE);
            final ArrayList<VCVideoItem> items = new ArrayList();
            if (json != null) {

                JSONArray jsonArray = json.getJSONArray("items");

                for (int i = 0; i < jsonArray.length(); i++) {
                    try {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        if(jsonObject.isNull("playlistItems")) continue ;

                        JSONObject playList= jsonObject.getJSONObject("playlistItems");

                        JSONArray playListItems = playList.getJSONArray("items");

                        for (int j = 0; j  < playListItems.length(); j ++) {
                            JSONObject playlistItem = playListItems.getJSONObject(j);

                            JSONObject video = null;
                            JSONObject snippet = null;
                            if (!playlistItem.isNull("snippet")) {
                                snippet = playlistItem.getJSONObject("snippet");
                                if (!snippet.isNull("resourceId")) {
                                    video = snippet.getJSONObject("resourceId");
                                }
                            }
                            if (video != null) {
                                String title = snippet.getString("title");
                                String id = video.getString("videoId");
                                String thumbUrl = snippet.getJSONObject("thumbnails").getJSONObject("default").getString("url");

                                VCVideoItem item = new VCVideoItem(title, thumbUrl, id);
                                items.add(item);
                            }
                        }
                    } catch (Exception err) {
                        err.printStackTrace();
                    }
                }

                networkQueue.postMain(()->{
                    MainViewModel model = THIS.get();
                    if(model != null) {
                        model.itemsData.set(items);
                    }
                });

            }
        }
        catch(Exception err){
            err.printStackTrace();
            networkQueue.postMain(()->{
                MainViewModel model = THIS.get();
                if(model != null) {
                    model.itemsData.set(new ArrayList<>());
                }
            });
        }
    }

    public void loadImage(final String url){
        WeakReference<MainViewModel> THIS = new WeakReference<>(MainViewModel.this);
        loadImage(THIS,url);
    }

    public void loadImage(WeakReference<MainViewModel> THIS,final String url){
        if(URLUtil.isValidUrl(url)) {
            networkQueue.post(() -> {
                try {
                    MainViewModel self = THIS.get();
                    if(self == null) return ;

                    Bitmap x;

                    HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
                    connection.connect();
                    InputStream input = connection.getInputStream();

                    x = BitmapFactory.decodeStream(input);
                    BitmapDrawable drawable = new BitmapDrawable(context.getResources().getSystem(), x);
                    if (drawable != null) {

                        self.networkQueue.postMain(() -> {
                            MainViewModel self1 = THIS.get();
                            if(self1 == null) return ;
                            self1.images.get().put(url, drawable);
                            self1.contentChangeListener.run();
                        });
                    }

                    input.close();
                } catch (Exception err) {
                    err.printStackTrace();
                }
            });
        }
    }

    public void setContext(Context context) {
        this.context = context ;
    }

    public void addContentChangeObserver(Runnable contentChangeListener) {
       this.contentChangeListener = contentChangeListener ;

    }

    public BitmapDrawable getImage(String thumbUrl) {
        return thumbUrl !=null ? images.get().get(thumbUrl) : null ;
    }

    public void setSelected(VCVideoItem holder) {
        selectedItemInList.set(holder);
    }

    public void addSelectionListener(@NotNull @NonNull final SelectedItemListener listener) {
        WeakReference<MainViewModel> THIS = new WeakReference<>(MainViewModel.this);
        selectedItemInList.addOnPropertyChangedCallback(new Observable.OnPropertyChangedCallback() {
            @Override
            public void onPropertyChanged(Observable sender, int propertyId) {
                MainViewModel model = THIS.get();
                if(model != null){
                    final VCVideoItem item =  model.selectedItemInList.get() ;
                    if(item != null) {
                        model.networkQueue.postMain(() -> listener.onSelected(item));
                    }
                }
            }
        });
    }
}